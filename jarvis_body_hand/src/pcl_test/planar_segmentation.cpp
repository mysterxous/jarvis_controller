#include <iostream>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/visualization/cloud_viewer.h>

#include <pcl/filters/passthrough.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/surface/convex_hull.h>
int
 main (int argc, char** argv)
{
  // pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);

  // // Fill in the cloud data
  // cloud->width  = 15;
  // cloud->height = 1;
  // cloud->points.resize (cloud->width * cloud->height);

  // // Generate the data
  // for (size_t i = 0; i < cloud->points.size (); ++i)
  // {
  //   cloud->points[i].x = 1024 * rand () / (RAND_MAX + 1.0f);
  //   cloud->points[i].y = 1024 * rand () / (RAND_MAX + 1.0f);
  //   cloud->points[i].z = 1.0;
  // }

  // // Set a few outliers
  // cloud->points[0].z = 2.0;
  // cloud->points[3].z = -2.0;
  // cloud->points[6].z = 4.0;

  // std::cerr << "Point cloud data: " << cloud->points.size () << " points" << std::endl;
  // for (size_t i = 0; i < cloud->points.size (); ++i)
  //   std::cerr << "    " << cloud->points[i].x << " "
  //                       << cloud->points[i].y << " "
  //                       << cloud->points[i].z << std::endl;

  pcl::PointCloud <pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud <pcl::PointXYZ>);
  // pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_projected (new pcl::PointCloud<pcl::PointXYZ>);

  if ( pcl::io::loadPCDFile <pcl::PointXYZ> ("/home/fx/mpc_ws/src/jarvis_controller/jarvis_body_hand/src/region_growing_rgb_tutorial.pcd", *cloud) == -1 )
  {
    std::cout << "Cloud reading failed." << std::endl;
    return (-1);
  }

  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  // Create the segmentation object
  pcl::SACSegmentation<pcl::PointXYZ> seg;
  // Optional
  seg.setOptimizeCoefficients (true);
  // Mandatory
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (0.01);

  seg.setInputCloud (cloud);
  seg.segment (*inliers, *coefficients);


// // Create a Convex Hull representation of the projected inliers
//   pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_hull (new pcl::PointCloud<pcl::PointXYZ>);
//   pcl::ConvexHull<pcl::PointXYZ> chull;
//   chull.setInputCloud (cloud);
//   chull.reconstruct (*cloud_hull);


  // Project the model inliers
  pcl::ProjectInliers<pcl::PointXYZ> proj;
  proj.setModelType (pcl::SACMODEL_PLANE);
  proj.setInputCloud (cloud);
  proj.setModelCoefficients (coefficients);
  proj.filter (*cloud_projected);

  



  if (inliers->indices.size () == 0)
  {
    PCL_ERROR ("Could not estimate a planar model for the given dataset.");
    return (-1);
  }

  // std::cerr << "Model coefficients: " << coefficients->values[0] << " " 
  //                                     << coefficients->values[1] << " "
  //                                     << coefficients->values[2] << " " 
  //                                     << coefficients->values[3] << std::endl;

  // std::cerr << "Model inliers: " << inliers->indices.size () << std::endl;
  // for (size_t i = 0; i < inliers->indices.size (); ++i)
  //   std::cerr << inliers->indices[i] << "    " << cloud->points[inliers->indices[i]].x << " "
  //                                              << cloud->points[inliers->indices[i]].y << " "
  //                                              << cloud->points[inliers->indices[i]].z << std::endl;

   //... populate cloud
   pcl::visualization::CloudViewer viewer ("Simple Cloud Viewer");
   viewer.showCloud (cloud_projected);  
   while (!viewer.wasStopped ())
   {
   }


  pcl::PCDWriter writer;
  writer.write ("table_scene_mug_stereo_textured_hull.pcd", *cloud_projected, false);

  return (0);
}