#include <pcl/io/openni_grabber.h>
#include <pcl/visualization/cloud_viewer.h>

#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <pcl/filters/passthrough.h>

#include <pcl/common/common.h>

// #include "jarvis_body_hand/realsense_device.h"

class RealsenseViewer
{
 public:
   RealsenseViewer () : viewer ("PCL realsense Viewer") 
   {
    // Clipping plane [near,far] 1.43245, 2.26685
    // Focal point [x,y,z] -0.0580136, 0.189062, 0.706522
    // Position [x,y,z] 0.000820556, 0.259412, -1.09695
    // View up [x,y,z] -0.00746042, -0.999203, -0.0392201
    // Camera view angle [degrees] 30
    // Window size [x,y] 960, 540
    // Window position [x,y] 65, 52
    viewer.setCameraPosition(
                              0.000820556, 0.259412, -1.09695, 
                              -0.00746042, -0.999203, -0.0392201);
   }

   void cloud_cb_ (const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr &cloud)
   {

     if (!viewer.wasStopped())
      {
        
 

        pcl::PCDWriter writer;

        pcl::VoxelGrid<pcl::PointXYZRGBA> vg;
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZRGBA>), cloud_f (new pcl::PointCloud<pcl::PointXYZRGBA>);


        //LIMIT PCL Z AXIS FROM 0 TO 1.2
        pcl::PassThrough<pcl::PointXYZRGBA> pass;
        pass.setInputCloud (cloud);
        pass.setFilterFieldName ("z");
        pass.setFilterLimits (0.0, 1.2);
        //pass.setFilterLimitsNegative (true);
        pass.filter (*cloud_filtered);




        // Create the filtering object: downsample the dataset using a leaf size of 1cm
        vg.setInputCloud (cloud_filtered);
        vg.setLeafSize (0.005f, 0.005f, 0.005f);
        vg.filter (*cloud_filtered);
        // std::cout << "PointCloud after filtering has: " << cloud_filtered->points.size ()  << " data points." << std::endl; //*


        
        // Create the segmentation object for the planar model and set all the parameters
        pcl::SACSegmentation<pcl::PointXYZRGBA> seg;
        pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
        pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_plane (new pcl::PointCloud<pcl::PointXYZRGBA> ());
        
        seg.setOptimizeCoefficients (true);
        seg.setModelType (pcl::SACMODEL_PLANE);
        seg.setMethodType (pcl::SAC_RANSAC);
        seg.setMaxIterations (100);
        seg.setDistanceThreshold (0.008); //less is fine




        viewer.removePointCloud("gg");
        viewer.addPointCloud (cloud_filtered, "gg");


        int i=0, nr_points = (int) cloud_filtered->points.size ();
        while (cloud_filtered->points.size () > 0.3 * nr_points)
        {
          // Segment the largest planar component from the remaining cloud
          seg.setInputCloud (cloud_filtered);
          seg.segment (*inliers, *coefficients);
          if (inliers->indices.size () == 0)
          {
            // std::cout << "Could not estimate a planar model for the given dataset." << std::endl;
            break;
          }

          // Extract the planar inliers from the input cloud
          pcl::ExtractIndices<pcl::PointXYZRGBA> extract;
          extract.setInputCloud (cloud_filtered);
          extract.setIndices (inliers);
          extract.setNegative (false);

          // Get the points associated with the planar surface
          extract.filter (*cloud_plane);
          // std::cout << "PointCloud representing the planar component: " << cloud_plane->points.size () << " data points." << std::endl;

          // Remove the planar inliers, extract the rest
          extract.setNegative (true);
          extract.filter (*cloud_f);
          *cloud_filtered = *cloud_f;
        }


        std::stringstream name;
        name << "scene_kinect" << ".pcd";
        writer.write<pcl::PointXYZRGBA> (name.str (), *cloud_filtered, false);
        // viewer.showCloud (cloud_filtered);

        // // Define R,G,B colors for the point cloud
        // pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGBA> source_cloud_color_handler (cloud_filtered, 255, 255, 255);
        // // We add the point cloud to the viewer and pass the color handler
        // viewer.addPointCloud (cloud_filtered, source_cloud_color_handler, "original_cloud");

        //Static remove Outlier
        pcl::StatisticalOutlierRemoval<pcl::PointXYZRGBA> sor;
        sor.setInputCloud (cloud_filtered);
        sor.setMeanK (50);
        sor.setStddevMulThresh (0.1);
        sor.filter (*cloud_filtered);


        // Creating the KdTree object for the search method of the extraction
        pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGBA>);
        tree->setInputCloud (cloud_filtered);

        std::vector<pcl::PointIndices> cluster_indices;
        pcl::EuclideanClusterExtraction<pcl::PointXYZRGBA> ec;
        ec.setClusterTolerance (0.02); // 2cm
        ec.setMinClusterSize (100);
        ec.setMaxClusterSize (25000);
        ec.setSearchMethod (tree);
        ec.setInputCloud (cloud_filtered);
        ec.extract (cluster_indices);

        int j = 0;
        for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
        {
          if(j>2) break;
          pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZRGBA>);
          for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
            cloud_cluster->points.push_back (cloud_filtered->points[*pit]); //*
          cloud_cluster->width = cloud_cluster->points.size ();
          cloud_cluster->height = 1;
          cloud_cluster->is_dense = true;


          



          // std::cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size () << " data points." << std::endl;
          // std::stringstream ss;
          // ss << "cloud_cluster_" << j << ".pcd";
          // writer.write<pcl::PointXYZRGBA> (ss.str (), *cloud_cluster, false); //*


          pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGBA> transformed_cloud_color_handler (cloud_cluster, 50/(j+1), 50*(j+1), 20); // Red
          viewer.removePointCloud("clust"+std::to_string(j));
          // viewer.addPointCloud (cloud_cluster, transformed_cloud_color_handler, "clust"+std::to_string(j));
          viewer.addPointCloud (cloud_cluster, "clust"+std::to_string(j));
          viewer.setPointCloudRenderingProperties (
                  pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "clust"+std::to_string(j));

          pcl::PointXYZRGBA minPt, maxPt;
          pcl::getMinMax3D (*cloud_cluster, minPt, maxPt);

          Eigen::Vector4f centroid; 
          pcl::compute3DCentroid(*cloud_cluster, centroid); 

          // std::cout << maxPt.x << "\t" << maxPt.y << "\t" << maxPt.z << "\n";
          double size = sqrt( (maxPt.x-minPt.x)*(maxPt.x-minPt.x) 
                    + (maxPt.y-minPt.y)*(maxPt.y-minPt.y) 
                    + (maxPt.z-minPt.z)*(maxPt.z-minPt.z) );
          std::cout << size <<"\n";

          viewer.removeShape("cube"+std::to_string(j)); 
          viewer.addSphere  ( pcl::PointXYZ(centroid[0],centroid[1],centroid[2]),
            size*0.5,
            "cube"+std::to_string(j)); 
          viewer.setShapeRenderingProperties(
                  pcl::visualization::PCL_VISUALIZER_REPRESENTATION, 
                  pcl::visualization::PCL_VISUALIZER_REPRESENTATION_WIREFRAME, 
                  "cube"+std::to_string(j)); 
          
          // viewer.addCube (Eigen::Vector3f( (maxPt.x+minPt.x)*0.5, 
          //                                  (maxPt.y+minPt.y)*0.5, 
          //                                  (maxPt.z+minPt.z)*0.5 ), 
          //                 Eigen::Quaternionf(1,0,0,0), 
          //                 maxPt.x-minPt.x, 
          //                 maxPt.y-minPt.y, 
          //                 maxPt.z-minPt.z,
          //                 "cube"+std::to_string(j));


          j++;
        }





        viewer.spinOnce ();
















       // viewer.showCloud (cloud_filtered);
      }
   }

    void run ()
    {
     pcl::Grabber* interface = new pcl::OpenNIGrabber();

     boost::function<void (const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr&)> f =
       boost::bind (&RealsenseViewer::cloud_cb_, this, _1);

     interface->registerCallback (f);

     interface->start ();

     while (!viewer.wasStopped())
     {
      
       boost::this_thread::sleep (boost::posix_time::seconds (1));
     }

     interface->stop ();
    }
    pcl::visualization::PCLVisualizer viewer;

    

   // pcl::visualization::CloudViewer viewer;
};

int main ()
{
 RealsenseViewer v;
 v.run ();
 return 0;
}













