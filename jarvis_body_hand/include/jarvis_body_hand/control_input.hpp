#pragma once
/*
 * Description : Control input of jarvis
 * Author      : Thanabadee Bulunseechart
 */

#include <ros/ros.h>
#include <ros/package.h>
#include <sensor_msgs/Joy.h>

#include "jarvis_body_hand/common.hpp"
#include "jarvis_body_hand/voice.hpp"

namespace jarvis_body_hand {

/*!
 * Class containing the algorithmic part of the package.
 */
class ControlInput
{

	private:
		ros::Subscriber subJoy_;
		ros::NodeHandle& nodeHandle_;

		Voice*  voice_;

		double CONTROL_HEAD_SENSE;
		bool bInit;

		void joyCb(const sensor_msgs::Joy::ConstPtr& msg);
		void readParameters();
	public:
		/*!
		 * Constructor.
		 */
		ControlInput(ros::NodeHandle& nodeHandle, 
								 Voice* voice);

		void GetHeadControl(double head_vel_control[2]);
		void ModeManager();
		uint8_t GetDriveMode() {return DRIVE_MODE;};
		uint8_t GetHeadMode() {return HEAD_MODE;};

		sensor_msgs::Joy RC;

		uint8_t DRIVE_MODE;
		uint8_t HEAD_MODE;
		/*!
		 * Destructor.
		 */
		virtual ~ControlInput();
};

} /* namespace */
